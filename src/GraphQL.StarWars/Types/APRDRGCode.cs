﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class APRDRGCode : StandardCode
    {
        public decimal weight { get; set; }
        public decimal alos { get; set; }
        public decimal glos { get; set; }
        public decimal losOutlierThreshold { get; set; }
        public string mdcCode { get; set; }
        public string type { get; set; }
    }
}
