﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class StandardCode
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Version Version { get; set; }
        public string DimensionName { get; set; }
    }
}
