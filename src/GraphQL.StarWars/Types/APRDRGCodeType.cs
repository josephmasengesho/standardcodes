﻿
using GraphQL.Types;

namespace GraphQL.StarWars.Types
{
    class APRDRGCodeType : ObjectGraphType<APRDRGCode>
    {
        public APRDRGCodeType()
        {
            Name = "APRDRGCode";
            Description = "Official Name of the APRDRGCode";

            Field(d => d.DimensionName).Description("The dimension of the code");
            Field(d => d.Description).Description("The name of code");
            Field(d => d.Code).Description("The code value");
            Field(d => d.Version.Year).Description("The code version");
            Field(d => d.weight).Description("Weight.");
            Field(d => d.alos).Description("ALOS");
            Field(d => d.mdcCode).Description("MDCCode");
            Field(d => d.type).Description("Code Type");
            Field(d => d.glos).Description("GLOS");
            Field(d => d.losOutlierThreshold).Description("LOSOutlierThreshold");
        }
    }
}
