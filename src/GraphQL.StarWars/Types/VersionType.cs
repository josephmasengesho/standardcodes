﻿
using GraphQL.Types;

namespace GraphQL.StarWars.Types
{
    public class VersionType : ObjectGraphType<Version>
    {
        public VersionType()
        {
            Name = "APRDRGCode";
            Description = "Official Name of the APRDRGCode";

            //Field(d => d.VersionName).Description("The the version name.");
            //Field(d => d.VersionYear).Description("The version year.");
        }
    }
}
