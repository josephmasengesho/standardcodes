﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.StarWars.Types;

namespace GraphQL.StarWars
{
    public class APRDRGData
    {
        private readonly List<APRDRGCode> _codes = new List<APRDRGCode>();
        private readonly List<Version> _versions = new List<Version>();

        public APRDRGData()
        {

            _codes.Add(new APRDRGCode
            {
                DimensionName = "APRDRG",
                Version = new Version  {  Id = 1, Year = 2017 },
                Code = "00111",
                Description = "LIVER TRANSPLANT &/OR INTESTINAL TRANSPLANT",
                weight = 17.158M,
                alos = 9.512M,
                glos = 7.89M,
                losOutlierThreshold = 15.14M,
                mdcCode  = "00",
                type ="P"

            });

            _codes.Add(new APRDRGCode
            {
                DimensionName = "APRDRG",
                Version = new Version { Id = 1, Year = 2017 },
                Code = "00222",
                Description = "PANCREAS TRANSPLANT",
                weight = 12.541M,
                alos = 1.543M,
                glos = 3.764M,
                losOutlierThreshold = 2.105M,
                mdcCode = "22",
                type = "M"

            });

            _codes.Add(new APRDRGCode
            {
                DimensionName = "APRDRG",
                Version = new Version { Id = 1, Year = 2017 },
                Code = "00333",
                Description = "SPINAL PROCEDURES",
                weight = 13.54M,
                alos = 1.58M,
                glos = 6.88M,
                losOutlierThreshold = 12.456M,
                mdcCode = "24",
                type = "M"

            });
        }

        public Task<APRDRGCode> GetCodeByCodeAsync(string code)
        {
            return Task.FromResult(_codes.FirstOrDefault(h => h.Code == code));
        }

        public Task<APRDRGCode> GetCodeByVersionAsync(int year)
        {
            return Task.FromResult(_codes.FirstOrDefault(h => h.Version.Year == year));
        }

        public Task<IEnumerable<APRDRGCode>> GetCodesByDimensionNameAsync(string name)
        {
            return Task.FromResult(_codes.Where(h => h.DimensionName == name));
        }
    }
}
