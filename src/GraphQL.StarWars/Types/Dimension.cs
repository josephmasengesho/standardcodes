﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class Dimension
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public List<Version> Versions { get; set; }
        public List<StandardCode> Codes { get; set; }
    }
}
