﻿using System;
using System.Collections.Generic;
using System.Text;
using GraphQL.Types;

namespace GraphQL.StarWars.Types
{
    public class Version : InterfaceGraphType<Version>
    {
        public int Id { get; set; }
        public int Year { get; set; } = 2017;
        //public string VersionName { get; set; } = "ICD11";
    }
}
