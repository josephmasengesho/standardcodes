﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.StarWars.Types;

namespace GraphQL.StarWars
{
    public class StandardCodesData
    {
        private readonly List<APRDRGCode> _aprdrgcodes = new List<APRDRGCode>();
        private readonly List<MSDRGCode> _msdrgcodes = new List<MSDRGCode>();

        public StandardCodesData()
        {

            _aprdrgcodes.Add(new APRDRGCode
            {
                DimensionName = "APRDRG",
                Version = new Version  {  Id = 1, Year = 2017 },
                Code = "00111",
                Description = "LIVER TRANSPLANT &/OR INTESTINAL TRANSPLANT",
                weight = 17.158M,
                alos = 9.512M,
                glos = 7.89M,
                losOutlierThreshold = 15.14M,
                mdcCode  = "00",
                type ="P"
            });

            _aprdrgcodes.Add(new APRDRGCode
            {
                DimensionName = "APRDRG",
                Version = new Version { Id = 1, Year = 2017 },
                Code = "00222",
                Description = "PANCREAS TRANSPLANT",
                weight = 12.541M,
                alos = 1.543M,
                glos = 3.764M,
                losOutlierThreshold = 2.105M,
                mdcCode = "22",
                type = "M"
            });

            _aprdrgcodes.Add(new APRDRGCode
            {
                DimensionName = "APRDRG",
                Version = new Version { Id = 1, Year = 2017 },
                Code = "00333",
                Description = "SPINAL PROCEDURES",
                weight = 13.54M,
                alos = 1.58M,
                glos = 6.88M,
                losOutlierThreshold = 12.456M,
                mdcCode = "24",
                type = "M"
            });

            _msdrgcodes.Add(new MSDRGCode
            {
                DimensionName = "MSDRG",
                Version = new Version { Id = 1, Year = 2017 },
                Code = "006",
                Description = "LIVER TRANSPLANT",
                weight = 4.8095M,
                alos = 7.7M,
                glos = 8.5M,
                mdcCode = "00",
                type = "PRE",
                ccmccCategory = "W/O MCC",
                finalRulePostAcuteDRG = "No"
            });

            _msdrgcodes.Add(new MSDRGCode
            {
                DimensionName = "MSDRG",
                Version = new Version { Id = 1, Year = 2017 },
                Code = "007",
                Description = "LUNG TRANSPLANT",
                weight = 9.6933M,
                alos = 15.9M,
                glos = 18.5M,
                mdcCode = "00",
                type = "PRE",
                ccmccCategory = "W/O",
                finalRulePostAcuteDRG = "No"
            });
        }

        public Task<APRDRGCode> GetAPRDRGByCodeAsync(string code)
        {
            return Task.FromResult(_aprdrgcodes.FirstOrDefault(h => h.Code == code));
        }

        public Task<MSDRGCode> GetMSDRGByCodeAsync(string code)
        {
            return Task.FromResult(_msdrgcodes.FirstOrDefault(h => h.Code == code));
        }

        public Task<APRDRGCode> GetCodeByVersionAsync(int year)
        {
            return Task.FromResult(_aprdrgcodes.FirstOrDefault(h => h.Version.Year == year));
        }

        public Task<IEnumerable<APRDRGCode>> GetCodesByDimensionNameAsync(string name)
        {
            return Task.FromResult(_aprdrgcodes.Where(h => h.DimensionName == name));
        }
    }
}
