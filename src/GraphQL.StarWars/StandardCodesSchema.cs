﻿using System;
using GraphQL.Types;

namespace GraphQL.StarWars
{
    public class StandardCodesSchema : Schema
    {
        public StandardCodesSchema(Func<Type, GraphType> resolveType)
            : base(resolveType)
        {
            Query = (StandardCodesQuery)resolveType(typeof(StandardCodesQuery));
        }
    }
}