﻿using GraphQL.StarWars.Types;
using GraphQL.Types;

namespace GraphQL.StarWars
{
    public class StandardCodesQuery : ObjectGraphType<object>
    {
        public StandardCodesQuery(StandardCodesData data)
        {
            Name = "Query";
            
            Field<ListGraphType<APRDRGCodeType>>(
                "codes",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "dimName", Description = "the standard code value" }
                ),
                resolve: context => data.GetCodesByDimensionNameAsync(context.GetArgument<string>("dimName"))
            );

            Field<MSDRGCodeType>(
                "msdrgcode",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "code", Description = "the standard code value" }
                ),
                resolve: context => data.GetMSDRGByCodeAsync(context.GetArgument<string>("code"))
            );
        }        
    }
}
